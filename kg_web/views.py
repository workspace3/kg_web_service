import os
import sys
from django.shortcuts import render

# Create your views here.

from django.http import HttpResponse

import json
from datetime import datetime
import timeit

project_path = os.path.normpath(
    os.path.join(os.path.dirname(os.path.abspath(__file__)), '..')
)
sys.path.insert(0, project_path)

from kg_construction.src.generate_kg import KgGenerator
from kg_construction.src.upload_kg import KgUploader
from kg_construction.utils.json_lines import load
from kg_web_service.settings import EXCHANGE_DIR


def hello_world(request):
    """
    hello world测试页面
    :param request:
    :return:
    """
    return HttpResponse("hello world!")


def kg_web(request):
    """
    展示首页
    :param request:
    :return:
    """
    #
    return render(request, "kg_web.html")


def upload(request):
    """
    上传文件
    :param request:
    :return:
    """
    print(request)
    files = request.FILES
    uploaded_file_names = []
    for item in files:
        f_obj = files.get(item)
        f_name = os.path.join(EXCHANGE_DIR, f_obj.name)
        with open(f_name, "w", encoding="utf8")as fo:
            for line in f_obj.chunks():
                fo.write(line.decode("utf8"))
        uploaded_file_names.append(os.path.basename(f_name))
    upload_return_msg = ["{}, 上传完成{}个文件.\n{}".format(
        datetime.now(), len(uploaded_file_names), "\n".join(uploaded_file_names))]
    return render(request, "kg_web.html", {"upload_return_msg": upload_return_msg})


def kg_construct(request):
    """
    创建图谱，并返回结果相关消息
    :param request:
    :param data_file:
    :return:
    """
    print("数据文件：{}".format(request.POST.get("select")))
    print("数据库名：{}".format(request.POST.get("db_name")))
    s = timeit.default_timer()
    selected_file = os.path.join(EXCHANGE_DIR, request.POST.get("select"))
    inputs = load(selected_file)
    collections = KgGenerator().run(inputs=inputs)
    new_db_name = request.POST.get("db_name")
    result = KgUploader().run(inputs=collections, database_name=new_db_name)
    print(json.dumps(result, ensure_ascii=False, indent=2))
    e = timeit.default_timer()
    if result.get("code"):
        msg = ["{0}, 创建失败, 耗时: {1:.3f} 秒.".format(datetime.now(), e - s)]
    else:
        msg = ["{0}, 创建成功, 耗时: {1:.3f} 秒.\n".format(datetime.now(), e - s)]
    url = ["http://192.168.1.4:8529"]
    return render(request, "kg_web.html", {"construction_return_msg": msg, "db_url": url})
