from django.contrib import admin
from .models import Records
# Register your models here.


class RecordsAdmin(admin.ModelAdmin):
    list_display = ("db_name", "desc", "created_time")
    list_display_links = ("db_name", )


admin.site.register(Records, RecordsAdmin)
