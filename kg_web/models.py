from django.db import models

# Create your models here.


class Records(models.Model):
    db_name = models.CharField("数据库名称", max_length=30)
    desc = models.TextField("数据库描述", max_length=200, blank=True)
    created_time = models.DateTimeField("创建时间")

    class Meta:
        verbose_name = "数据库创建日志"
        verbose_name_plural = "数据库创建日志"

    def __set__(self):
        return self.db_name
