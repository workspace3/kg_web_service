# -*- coding: utf-8 -*-

from pypinyin import lazy_pinyin


def han2pinyin(s):
    result = "".join([x.capitalize() for x in lazy_pinyin(s, errors='ignore')])
    if len(result) == 0:
        return s
    return result


if __name__ == "__main__":
    s = "hello任务"
    result = han2pinyin(s)
    print(result)
