# -*- coding: utf-8 -*-
# @Time    : 2019/08/19
# @Author  : wen

import json


def load(f):
    data = []
    with open(f, encoding="utf8")as fi:
        for line in fi.readlines():
            line = line.rstrip()
            data.append(json.loads(line))
    return data


def dump(o, f):
    with open(f, "w", encoding="utf8")as fo:
        for line in o:
            fo.write(json.dumps(line, ensure_ascii=False)+"\n")
