# -*- coding: utf-8 -*-
from arango import ArangoClient
import time
import os

from ..conf.config import Config


class DbConn(object):
    def __init__(self, db_info):
        self.db_info = db_info
        self.client = ArangoClient(host=self.db_info["dbhost"],
                                   port=self.db_info["dbport"])
        self.db = None

    def connect_db(self, db_name):
        try:
            self.db = self.client.db(db_name,
                                     username=self.db_info["dbuser"],
                                     password=self.db_info["dbpwd"])
            return True
        except:
            return False

    def create_db(self, new_db_name, ignore=False):
        if not self.connect_db("_system"):
            return False
        if not self.db.has_database(new_db_name) or ignore:
            try:
                if not self.delete_db(new_db_name):
                    return False
                self.db.create_database(new_db_name,
                                        users=[{'username': self.db_info["dbuser"],
                                                'password': self.db_info["dbpwd"],
                                                'active': True}])
                self.db.update_permission(username=self.db_info["dbuser"],
                                          permission='rw',
                                          database=new_db_name)
                return True
            except Exception as e:
                print(e.__str__())
                return False
        else:
            return False

    def delete_db(self, db_name):
        if not self.connect_db("_system"):
            return False
        if self.db.has_database(db_name):
            try:
                self.db.delete_database(db_name)
                return True
            except:
                return False
        return True

    def get_db_list(self):
        if not self.connect_db("_system"):
            raise Exception("Failed to get db list.")
        result = []
        for db_name in self.db.databases():
            if "_system" == db_name:
                continue
            self.connect_db(db_name)
            result.append({
                "name": self.db.name,
                "created": self.db.details()["build-date"]
            })
        return result

    # 以下成员函数均为db内的操作，所以调用前，首先要显式调用self.connect_db()
    #
    def create_graph(self, graph_name, combs):
        # combs: [[head, tail, edge],...]
        try:
            if not self.db.has_graph(graph_name):
                graph = self.db.create_graph(graph_name)
                for head_vertex, tail_vertex, edge in combs:
                    if not graph.has_vertex_collection(head_vertex):
                        _ = graph.create_vertex_collection(head_vertex)
                    if not graph.has_vertex_collection(tail_vertex):
                        _ = graph.create_vertex_collection(tail_vertex)
                    if not graph.has_edge_definition(edge):
                        _ = graph.create_edge_definition(edge_collection=edge,
                                                         from_vertex_collections=[head_vertex],
                                                         to_vertex_collections=[tail_vertex])
            return True
        except:
            return False

    def insert_graph_edge(self, graph_name, edge, documents):
        def insert_single(document):
            if not self.db.has_graph(graph_name):
                raise Exception("Graph: {} , is not found in db.".format(graph_name))
            else:
                graph = self.db.graph(graph_name)
                edge_collections = graph.edge_collection(edge)
                if not edge_collections.has(document["_key"]):
                    edge_collections.insert(document)
        #
        try:
            if isinstance(documents, list):
                for d in documents:
                    insert_single(d)
                    time.sleep(0.005)
            else:
                insert_single(documents)
            return True
        except:
            return False

    def insert_graph_vertex(self, graph_name, vertex, documents):
        def insert_single(document):
            if not self.db.has_graph(graph_name):
                raise Exception("Graph: {} , is not found in db.".format(graph_name))
            else:
                graph = self.db.graph(graph_name)
                vertex_collections = graph.vertex_collection(vertex)
                if not vertex_collections.has(document["_key"]):
                    vertex_collections.insert(document)
        #
        try:
            if isinstance(documents, list):
                for d in documents:
                    insert_single(d)
                    time.sleep(0.005)
            else:
                insert_single(documents)
            return True
        except:
            return False

    def create_collection(self, collection_name):
        try:
            _ = self.db.create_collection(collection_name)
            return True
        except:
            return False

    def insert_documents(self, collection_name, documents):
        try:
            collection = self.db.collection(collection_name)
            collection.insert_many(documents)
            return True
        except:
            return False

    def get_collection_list(self):
        return self.db.collections()


def get_db_info():
    curr_file = os.path.abspath(__file__)
    project_path = os.path.normpath(os.path.join(os.path.dirname(curr_file), ".."))
    conf = Config(project_path, "conf/env_config.ini")
    db_info = {"dbhost": conf.get_value("db", "arango_dbhost"),
               "dbport": conf.get_value("db", "arango_dbport"),
               "dbuser": conf.get_value("db", "arango_dbuser"),
               "dbpwd": conf.get_value("db", "arango_dbpwd")}
    return db_info


# test
def test():
    import json
    db_info = get_db_info()
    conn = DbConn(db_info)
    print(json.dumps(conn.get_db_list(), indent=2))

    # db_name = "learn"
    # conn.connect_db(db_name)
    # graph_name = "people_liveIn_location"
    # relation_name = "liveIn"
    # head_name = "people"
    # tail_name = "location"
    # conn.create_graph(graph_name, head_name, tail_name, relation_name)
    # people_collections = [
    #     {"_key": "h1", "name": "李雷"},
    #     {"_key": "h2", "name": "李华"}
    # ]
    # location_collections = [
    #     {"_key": "t1", "name": "上海"},
    #     {"_key": "t2", "name": "深圳"}
    # ]
    # live_in_collections = [
    #     {"_key": "r1", "name": "居住地", "_from": "people/h1", "_to": "location/t1"},
    #     {"_key": "r2", "name": "居住地", "_from": "people/h1", "_to": "location/t2"},
    #     {"_key": "r3", "name": "居住地", "_from": "people/h2", "_to": "location/t2"}
    # ]
    # conn.insert_graph_vertex(graph_name, head_name, people_collections)
    # conn.insert_graph_vertex(graph_name, tail_name, location_collections)
    # conn.insert_graph_edge(graph_name, relation_name, live_in_collections)


# main
if __name__ == "__main__":
    test()
