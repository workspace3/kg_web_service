# _*_ coding: utf-8 _*_

import os
import sys
import json
import logging.config

from ..conf.config import Config

project_path = os.path.abspath(
    os.path.join(os.path.dirname(os.path.abspath(__file__)), '..'))
sys.path.insert(0, project_path)

conf = Config(project_path, 'core/conf/env_config.ini')


def setup_logging(default_level=logging.INFO, env_key='LOG_CFG'):
    """
    :param default_level: logging.INFO、logging.WARNING、logging.ERROR
    :param env_key:
    :return:
    """
    path = os.path.normpath(os.path.join(
        project_path,
        conf.get_value(
            'log',
            'log_conf_path')))
    log_path = os.path.normpath(os.path.join(
        project_path, conf.get_value(
            'log', 'log_path')))
    value = os.getenv(env_key, None)
    if value:
        path = value
    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = json.load(f)
        try:
            config['handlers']['info_file_handler']['filename'] = log_path + '/info.log'
            config['handlers']['warning_file_handler']['filename'] = log_path + '/warn.log'
            config['handlers']['error_file_handler']['filename'] = log_path + '/errors.log'
        except Exception as e:
            print(e)
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)
