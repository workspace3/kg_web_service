# _*_ coding: utf-8 _*_

import os
import logging
import traceback

from configparser import ConfigParser

project_path = os.path.normpath(
    os.path.join(os.path.dirname(os.path.abspath(__file__)), '../../')
)

env_dict = {
    '0': 'internal',
    '1': 'pre_production',
    '2': 'production'
}


class Config(object):
    def __init__(self, project_path, conf_path):
        self._project_path = project_path
        self._conf_path = os.path.join(self._project_path, conf_path)
        self.parser = ConfigParser()
        self.parser.read(self._conf_path)

    def get_value(self, section, key):
        if section == 'environment':
            environment = os.environ.get("ENVIRONMENT")
            if environment is None:
                pass
            else:
                if environment in env_dict:
                    return env_dict[environment]
                else:
                    return env_dict['2']
        try:
            res = str(self.parser.get(section, key))
            return res
        except Exception as err:
            logging.error(traceback.format_exc(err))

    def get_items(self, section):
        try:
            res = self.parser.items(section)
            return dict((x, y) for x, y in res)
        except Exception:
            logging.error(traceback.format_exc())

    def get_sections(self):
        try:
            res = self.parser.sections()
            return res
        except Exception:
            logging.error(traceback.format_exc())

    def get_options(self, section):
        try:
            res = self.parser.options(section)
            return res
        except Exception:
            logging.error(traceback.format_exc())
