# -*- coding: utf-8 -*-
# @Time    : 2019/08/29
# @Author  : wen

from ..utils.arango_db import DbConn, get_db_info
from ..utils.pinyin import han2pinyin
from ..src.base.name_transfer import name_cn2en


SPLIT_TAG_IN_GRAPH_NAME = "_"


class KgGenerator:
    """
    假设输入的数据与关系抽取的输出数据格式相同
    [
        {
            "text": "李梅梅出生在深圳",
            "predict": [
                    {
                        "head": "李梅梅", "head_type": "人物", "head_id": "e_5",
                        "tail": "深圳", "tail_type": "地点", "tail_id": "e_4",
                        "relation": "出生地"
                    },
                    ...
            ]
        },
        ...
    ]
    ======================================
    接口函数：
    -------------------------------
    run(inputs, file_name="")
    功能： 处理输入的三元组数据生成collections保存到为文件，并上传到腾讯云
    入参说明：
        inputs：三元组数据，list，不可缺省。
    出参说明：
        return_dict：返回文件列表，dict。key=区别不同文件的关键词，value=文件url
    """
    def __init__(self):
        db_info = get_db_info()
        self.db_conn = DbConn(db_info)

    def run(self, inputs):
        # 提取出三元组数据
        new_inputs = []
        for single_input in inputs:
            new_inputs.extend(single_input["predict"])
        # 生成各个collection
        all_collections = self._generate_collections(new_inputs)
        collections_to_json = {}
        for collection_name in all_collections:
            for graph_name in all_collections[collection_name]:
                tmp = all_collections[collection_name][graph_name]["data"]
                if "edge" == all_collections[collection_name][graph_name]["category"]:
                    if graph_name not in collections_to_json:
                        collections_to_json[graph_name] = []
                    # 去重
                    for t in tmp:
                        if t not in collections_to_json[graph_name]:
                            collections_to_json[graph_name].append(t)
                else:
                    if collection_name not in collections_to_json:
                        collections_to_json[collection_name] = []
                    # 去重
                    for t in tmp:
                        if t not in collections_to_json[collection_name]:
                            collections_to_json[collection_name].append(t)
        return collections_to_json

    def _generate_collections(self, inputs):
        #
        def gen_new_vertex(_key, name, others=None):
            result = {"_key": _key, "name": name}
            if others is not None:
                result.update(others)
            return result
        #
        def gen_new_edge(_key, name, _from, _to, others=None):
            result = {"_key": _key, "name": name,
                      "_from": _from, "_to": _to}
            if others is not None:
                result.update(others)
            return result
        #
        def gen_key(num):
            def baseN(num, b=36):
                result = ((num == 0) and "0") or (
                            baseN(num // b, b).lstrip("0") + "0123456789abcdefghijklmnopqrstuvwxyz"[num % b])
                if len(result) == 1:
                    return '000' + result
                elif len(result) == 2:
                    return '00' + result
                elif len(result) == 3:
                    return '0' + result
                else:
                    return result
            return baseN(num)
        #
        keys = {}
        all_collections = {}
        #
        for single_input in inputs:
            # 将collection的名字换成英文，arangoDB不支持中文字符的命名
            head_name = name_cn2en(single_input["head_type"])
            if head_name is None:
                head_name = han2pinyin(single_input["head_type"])
            tail_name = name_cn2en(single_input["tail_type"])
            if tail_name is None:
                tail_name = han2pinyin(single_input["tail_type"])
            edge_name = name_cn2en(single_input["relation"])
            if edge_name is None:
                edge_name = han2pinyin(single_input["relation"])
            graph_name = SPLIT_TAG_IN_GRAPH_NAME.join([head_name, edge_name, tail_name])
            # 对dict新增的key进行初始化
            if head_name not in all_collections:
                all_collections[head_name] = {}
            if head_name not in keys:
                keys[head_name] = {"curr_key": 1, "history": {}}
            if graph_name not in all_collections[head_name]:
                all_collections[head_name][graph_name] = {"category": "vertex", "data": []}
            if tail_name not in all_collections:
                all_collections[tail_name] = {}
            if tail_name not in keys:
                keys[tail_name] = {"curr_key": 1, "history": {}}
            if graph_name not in all_collections[tail_name]:
                all_collections[tail_name][graph_name] = {"category": "vertex", "data": []}
            if edge_name not in all_collections:
                all_collections[edge_name] = {}
            if edge_name not in keys:
                keys[edge_name] = {"curr_key": 1, "history": {}}
            if graph_name not in all_collections[edge_name]:
                all_collections[edge_name][graph_name] = {"category": "edge", "data": []}
            # 生成对应的_key, _from, _to等数据
            # 判断头实体是否已经创建
            curr_keyword = str(single_input["head_id"])
            if curr_keyword in keys[head_name]["history"]:
                head_key = keys[head_name]["history"][curr_keyword]
            else:
                head_key = gen_key(keys[head_name]["curr_key"])
                keys[head_name]["curr_key"] += 1
                keys[head_name]["history"][curr_keyword] = head_key
            # 判断尾实体是否已经创建
            curr_keyword = str(single_input["tail_id"])
            if curr_keyword in keys[tail_name]["history"]:
                tail_key = keys[tail_name]["history"][curr_keyword]
            else:
                tail_key = gen_key(keys[tail_name]["curr_key"])
                keys[tail_name]["curr_key"] += 1
                keys[tail_name]["history"][curr_keyword] = tail_key
            # 判断边是否已经被创建
            curr_keyword = str(tuple([single_input["head_id"],
                                      single_input["tail_id"],
                                      single_input["relation"]] +
                                     [v for k, v in single_input.items() if k.startswith("relation_")]))
            if curr_keyword in keys[edge_name]["history"]:
                edge_key = keys[edge_name]["history"][curr_keyword]
            else:
                edge_key = gen_key(keys[edge_name]["curr_key"])
                keys[edge_name]["curr_key"] += 1
                keys[edge_name]["history"][curr_keyword] = edge_key
            _from = head_name + "/" + head_key
            _to = tail_name + "/" + tail_key
            # 将vertex和edge增加到对应的dict里
            head_attr = dict([(k[len("head_"):], v) for k, v in single_input.items()
                              if k.startswith("head_") and "head_type" != k])
            head_vertex = gen_new_vertex(head_key, single_input["head"], head_attr)
            tail_attr = dict([(k[len("tail_"):], v) for k, v in single_input.items()
                              if k.startswith("tail_") and "tail_type" != k])
            tail_vertex = gen_new_vertex(tail_key, single_input["tail"], tail_attr)
            edge_attr = dict([(k[len("relation_"):], v) for k, v in single_input.items()
                              if k.startswith("relation_")])
            edge = gen_new_edge(edge_key, single_input["relation"], _from, _to, edge_attr)
            if head_vertex not in all_collections[head_name][graph_name]["data"]:
                all_collections[head_name][graph_name]["data"].append(head_vertex)
            if tail_vertex not in all_collections[tail_name][graph_name]["data"]:
                all_collections[tail_name][graph_name]["data"].append(tail_vertex)
            if edge not in all_collections[edge_name][graph_name]["data"]:
                all_collections[edge_name][graph_name]["data"].append(edge)
        #
        return all_collections


if __name__ == "__main__":
    from pprint import pprint
    inputs = [
        {"text": "周杰伦(Jay Chou)，1979年1月18日出生于台湾省新北市，毕业于淡江中学，中国台湾流行乐男歌手、音乐人、演员、导演、编剧等。", "predict": [
            {"head": "周杰伦", "head_type": "人物", "head_id": "e_1", "head_age": "30",
             "tail": "淡江中学", "tail_type": "学校", "tail_id": "e_4",
             "relation": "毕业院校"},
            {"head": "周杰伦", "head_type": "人物", "head_id": "e_1", "head_age": "30",
             "tail": "中国", "tail_type": "国家", "tail_id": "e_5",
             "relation": "国籍"},
            {"head": "周杰伦", "head_type": "人物", "head_id": "e_2",
             "tail": "1979年1月18日", "tail_type": "Date", "tail_language": "中文",
             "tail_id": "e_2",
             "relation": "出生日期", "relation_type": "年月日"},
            {"head": "周杰伦", "head_type": "人物", "head_id": "e_2",
             "tail": "台湾省新北市", "tail_type": "地点", "tail_id": "e_3",
             "relation": "出生地", "relation_language": "中文"},
            {"head": "周杰伦", "head_type": "人物", "head_id": "e_2",
             "tail": "台湾省新北市", "tail_type": "地点", "tail_id": "e_3",
             "relation": "出生地", "relation_relation_language": "简体中文"}
        ]},
        {"text": "林俊杰(JJ Lin)，1981年3月27日出生于新加坡，华语流行乐男歌手、词曲创作者、音乐制作人。2003年发行首张创作专辑《乐行者》。2004年凭专辑《第二天堂》中的歌曲《江南》获得广泛关注。",
         "predict": [
             {"head": "乐行者", "head_type": "歌曲", "head_id": "e_6",
              "tail": "林俊杰", "tail_type": "人物", "tail_id": "e_7",
              "relation": "歌手"},
             {"head": "江南", "head_type": "歌曲", "head_id": "e_8",
              "tail": "林俊杰", "tail_type": "人物", "tail_id": "e_7",
              "relation": "歌手"},
             {"head": "乐行者", "head_type": "歌曲", "head_id": "e_6",
              "tail": "第二天堂", "tail_type": "音乐专辑",
              "tail_id": "e_11",
              "relation": "所属专辑"},
             {"head": "江南", "head_type": "歌曲", "head_id": "e_8",
              "tail": "第二天堂", "tail_type": "音乐专辑", "tail_id": "e_11",
              "relation": "所属专辑"},
             {"head": "林俊杰", "head_type": "人物", "head_id": "e_7",
              "tail": "1981年3月27日", "tail_type": "Date",
              "tail_id": "e_9",
              "relation": "出生日期"},
             {"head": "林俊杰", "head_type": "人物", "head_id": "e_7",
              "tail": "新加坡", "tail_type": "地点", "tail_id": "e_10",
              "relation": "出生地"}]}

    ]
    constructor = KgGenerator()
    pprint(constructor.run(inputs))
