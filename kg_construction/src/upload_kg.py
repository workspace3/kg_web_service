# -*- coding: utf-8 -*-
# @Time    : 2019/08/21
# @Author  : wen
import time

from ..utils.arango_db import DbConn, get_db_info

FILENAME_SPLIT = "_"
EDGE_NAME_SPLIT_NUM = 3
DB_PREFIX = ""
IGNORE = '0'
NOT_IGNORE = '1'


class KgUploader:
    """
    接口函数：
    -------------------------------
    run(inputs=None, file_name=None, database_name=None, ignore=NOT_IGNORE)
    功能： 将生成的collections导入到arangoDB中
    入参说明：
        inputs：collections数据，dict，直接从。key=collection file name，value=collection items。不可缺省。
        file_name：没有用到，只是为了和其他模块的run接口保持一致
        database_name：将要导入的数据库名称（用户命名），str，不可缺省。
                        只能以英文字母开头，除英文字母和阿拉伯数字外，只允许["-", "_"]两个特殊字符
        ignore：是否覆盖已有的同名数据库。0：覆盖，1：不覆盖。可缺省，默认是1。
    出参说明：
        return_dict：返回标识码，0：正常导入，1：因已有同名数据库而报错，2：其他错误导致上传失败
    -------------------------------
    PS：调用upload时，可以先ignore=False调用一次，如果有同名数据库那么上传数据库失败，会返回2。
        此时，向用户确认，是否覆盖，用户选择覆盖的话则ignore=True再调用一次，即完成数据库覆盖上传。
        如果第一次调用返回0，那么正常创建数据库并完成数据上传。
    """
    def __init__(self):
        db_info = get_db_info()
        self.db_conn = DbConn(db_info)

    def run(self, inputs=None, database_name=None, ignore=NOT_IGNORE):
        if IGNORE == ignore:
            ignore = True
        else:
            ignore = False
        database_name = DB_PREFIX + database_name
        return_dict = {"code": 0}
        if inputs is None or database_name is None:
            return_dict["code"] = 2
            return return_dict
        is_success = self.db_conn.create_db(database_name, ignore=ignore)
        if not is_success:
            return_dict["code"] = 1
            return return_dict
        time.sleep(0.3)
        self.db_conn.connect_db(database_name)
        # 创建graph
        graph_name = "graph"
        combs = []
        for collection_name in inputs:
            # 获取文件名（无后缀）
            name = collection_name
            try:
                # 如果是“边”的collection，那么文件名类似“sessionid_Person_isBirthPlace_Location”
                if EDGE_NAME_SPLIT_NUM == len(name.split(FILENAME_SPLIT)):
                    head_name, edge_name, tail_name = name.split(FILENAME_SPLIT)
                    combs.append([head_name, tail_name, edge_name])
            except Exception as e:
                print(e.__str__())
                return_dict["code"] = 2
                return return_dict
        _ = self.db_conn.create_graph(graph_name, combs)
        # 按照节点collections在前，边collections在后的顺序排序
        sorted_collection_names = sorted(inputs.keys(), key=lambda x: len(x.split(".")[0].split(FILENAME_SPLIT)))
        # 依次添加到ArangoDB中
        for collection_name in sorted_collection_names:
            name = collection_name
            try:
                # 如果是“边”的collection，那么文件名类似“Person_isBirthPlace_Location”
                if EDGE_NAME_SPLIT_NUM == len(name.split(FILENAME_SPLIT)):
                    head_name, edge_name, tail_name = name.split(FILENAME_SPLIT)
                    self.db_conn.insert_graph_edge(graph_name, edge_name, inputs[collection_name])
                # 否则为“结点”的collection，那么文件名类似“Person”
                else:
                    collection_name = name
                    self.db_conn.insert_graph_vertex(graph_name, collection_name, inputs[collection_name])
            except:
                return_dict["code"] = 2
                return return_dict
        return return_dict


if __name__ == "__main__":
    inputs = {
        "Person_isBirthPlace_Location": [
            {"_key": "0001", "name": "出生地", "_from": "Person/0002", "_to": "Location/0001", "language": "中文"},
            {"_key": "0002", "name": "出生地", "_from": "Person/0003", "_to": "Location/0002"}
        ],
        "Location": [
            {"_key": "0001", "name": "台湾省新北市", "id": "e_3"},
            {"_key": "0002", "name": "新加坡", "id": "e_10"}
        ],
        "Person": [
            {"_key": "0001", "name": "周杰伦", "id": "e_1", "age": "30"},
            {"_key": "0002", "name": "周杰伦", "id": "e_2"},
            {"_key": "0003", "name": "林俊杰", "id": "e_7"}
            ],
        "Date": [
            {"_key": "0001", "name": "1979年1月18日", "langugae": "中文", "id": "e_2"},
            {"_key": "0002", "name": "1981年3月27日", "id": "e_9"}
        ],
        "Nation": [
            {"_key": "0001", "name": "中国", "id": "e_5"}
        ],
        "Person_isDateOfBirth_Date": [
            {"_key": "0001", "name": "出生日期", "_from": "Person/0002", "_to": "Date/0001", "type": "年月日"},
            {"_key": "0002", "name": "出生日期", "_from": "Person/0003", "_to": "Date/0002"}
        ],
        "Person_isNationallity_Nation": [
            {"_key": "0001", "name": "国籍", "_from": "Person/0001", "_to": "Nation/0001"}
        ],
        "School": [
            {"_key": "0001", "name": "淡江中学", "id": "e_4"}
        ],
        "Person_isGraduatedSchool_School": [
            {"_key": "0001", "name": "毕业院校", "_from": "Person/0001", "_to": "School/0001"}
        ],
        "Song": [
            {"_key": "0001", "name": "乐行者", "id": "e_6"},
            {"_key": "0002", "name": "江南", "id": "e_8"}
        ],
        "Song_isSinger_Person": [
            {"_key": "0001", "name": "歌手", "_from": "Song/0001", "_to": "Person/0003"},
            {"_key": "0002", "name": "歌手", "_from": "Song/0002", "_to": "Person/0003"}
        ],
        "Album": [
            {"_key": "0001", "name": "第二天堂", "id": "e_11"}
        ],
        "Song_isInAlbum_Album": [
            {"_key": "0001", "name": "所属专辑", "_from": "Song/0001", "_to": "Album/0001"},
            {"_key": "0002", "name": "所属专辑", "_from": "Song/0002", "_to": "Album/0001"}
        ]
    }
    database_name = "test_2020"
    uploader = KgUploader()
    print(uploader.run(inputs=inputs, database_name=database_name, ignore=IGNORE))
