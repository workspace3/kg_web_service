# -*- coding: utf-8 -*-
# @Time    : 2019/08/21
# @Author  : wen

import os

curr_file = os.path.abspath(__file__)
project_path = os.path.normpath(os.path.join(os.path.dirname(curr_file), "../../../"))
__MAPPING_FILE__ = os.path.normpath(os.path.join(os.path.dirname(curr_file), "names.txt"))


def load_mapping(mapping_file):
    mapping = {}
    with open(mapping_file, encoding="utf8")as fi:
        for line in fi.readlines():
            line = line.rstrip()
            parts = line.split("\t")
            if len(parts) != 2:
                continue
            mapping[parts[0]] = parts[1]
    return mapping


mapping = load_mapping(__MAPPING_FILE__)


def name_cn2en(name):
    if name in mapping:
        return mapping[name]
    return None
